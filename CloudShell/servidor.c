#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>		/* O_RDONLY, O_WRONLY, O_CREAT, O_RDWR* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lista_users.h"

#define LER 0
#define ESCREVER 1
#define MAX 128
List users=NULL;
int cloudCom;
int temSaldoP;
int temSaldoR;

void lerPipe (char nome[], char stat[]) {
	char buf[2], novo[MAX];
	char *mat[2];
	int i=0;
	int fim=0;
	buf[1]=novo[0]='\0';

	while (!fim && read(cloudCom,buf,1)>0) {
    	
    	if (buf[0]==';') {
    		fim=1;
    	}
    	else {
			if(buf[0]!=':'){
        	    strcat(novo,buf);
         	}else{
            	mat[i++] = strdup(novo);
            	novo[0]='\0';
        	}
		}
    	mat[i]=strdup(novo);

	}
	nome=strcpy(nome,mat[0]);
	stat=strcpy(stat,mat[1]);
	//printf("mat:--%s--%s\n",mat[0],mat[1]);
	//printf("%s\n",comando);
}

void descontar(char nome[], char stat[]){
	double percent;
	percent = atof(stat);
	tiraSaldo(searchUser(users,nome),percent*0.01);
}

void temSaldo(){
	//printf("vou ver Saldo\n");
	char buf[2];
	char nome[MAX];
	buf[1]='\0';
	nome[0]='\0';
	int fim=0;
	while (!fim && read(temSaldoP,buf,1)>0){
		if(buf[0]!=';'){
			strcat(nome,buf);

		}else{
			fim=1;
		}

	}

	User utl = searchUser(users,nome);
	if(utl==NULL){
		users=addUser(users,nome,-1,120);
		write(temSaldoR,"1;",strlen("1;"));
		//printf("1\n");
	}
	else{
		//printf("%f\n",getSaldo(utl));
		if(getSaldo(utl)>0){
			write(temSaldoR,"1;",strlen("1;"));
			//printf("1\n");
		}else{
			write(temSaldoR,"0;",strlen("0;"));
			//printf("0\n");
		}
	}


}
void sigFim(int sig){
    saveList(users);
    exit(0);
    
}
int main(){
    signal(SIGINT,sigFim);
    users=loadList();
    
    //if (users==NULL) printf("Lista vazia\n");

	signal(SIGUSR1,temSaldo);
	char nome[MAX],stat[MAX];
	char id[6];
	sprintf(id,"%d;",getpid());
	cloudCom=open("/tmp/estatistica",O_RDWR);
	temSaldoP=open("/tmp/saldoP",O_RDWR);
	temSaldoR=open("/tmp/saldoR",O_RDWR);

	write(temSaldoR,id,strlen(id));

	
	while(1){
		lerPipe(nome,stat);
		//printf("nome:%s_stat_%s\n",nome,stat);
		descontar(nome,stat);
        saveList(users);
	}

}

