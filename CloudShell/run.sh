#!/bin/bash
 
input="user1
pwd
date
ls -l
exit"
 
echo "Compilando o programa..."
 
make exe > /tmp/compilando
rm /tmp/compilando
 
echo "Iniciando o servidor..."
 
./CloudShell & echo $! > /tmp/cloudshellPID
./servidor & echo $! > /tmp/servidorPID
 
 
echo "Correndo utilizador de teste..."
echo ""
./CloudShellCMD << EOF
$input
EOF
 
echo ""
echo "Fim do teste automático."
echo ""
 
while true; do
    read -p "Deseja testar o programa manualmente? (y/n) " yn
    case $yn in
        [Yy]* ) ./CloudShellCMD; break;;
        [Nn]* ) break;;
        * ) echo "Por favor responda y ou n";;
    esac
done
 
echo "A fechar processos abertos..."
 
PID1=$(cat /tmp/cloudshellPID)
PID2=$(cat /tmp/servidorPID)
 
kill $PID1
kill $PID2
 
echo "A eliminar ficheiros temporários..."
rm /tmp/cloudshellPID
rm /tmp/servidorPID