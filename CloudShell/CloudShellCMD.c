#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LER 0
#define ESCREVER 1
#define MAX 128


int main () {
	char nome[MAX], comando[MAX],tmp[MAX];
	int scomando, pipeR, lidos,fim,sair=0;

	if((scomando = open("/tmp/comando",O_WRONLY))<0 ){
        printf("Servidor não disponível\n");
        exit(-1);
    }

    printf("Diga o nome de utilizador\n");
    scanf(" %s",nome);

    while((strstr(nome,";")!=NULL) || (strstr(nome,":")!=NULL)){
        printf("o nome nao pode conter \";\" nem \":\".\n");
        printf("Diga o nome de utilizador de novo\n");
        scanf(" %s",nome);
    }
    
    
    strcpy(tmp,"/tmp/"); strcat(tmp,nome);

    if (mkfifo(tmp,0666)<0) {
    	close(scomando);
    	printf("Não foi possivel estabelecer ligação para leitura do servidor\n");
        exit(-1);
    }

	if ((pipeR=open(tmp,O_RDWR)) < 0) {
		close(scomando);
		printf("Erro na abertura do pipe\n");
        execlp("rm","rm",tmp,NULL);
        exit(-1);
	}
	

	do {
		printf("Introduza o comando:\n");
		scanf(" %99[^\n]",comando);
		if (strcmp(comando,"exit")) {
			char buf[2*MAX], bufR[MAX];
			strcpy(buf,nome); strcat(buf,":"); strcat(buf,comando); strcat(buf,";");
			/*printf("%s\n",buf);*/
			write(scomando,buf,strlen(buf));
			/*printf("mandei %s %d\n",buf,w);*/
			fim=0;
			while (!fim && (lidos=read(pipeR,bufR,MAX))>0) {
				if (strstr(bufR,"exit")==NULL) write(1,bufR,lidos);
				if(lidos<MAX) fim=1;
			}
			if (strstr(bufR,"exit")!=NULL) sair=1;
		} 
	}while(strcmp(comando,"exit") && !sair);
	
	

	close(scomando);
	close(pipeR);

	execlp("rm","rm",tmp,NULL);
	printf("Adeus\n");

	return 0;
}
