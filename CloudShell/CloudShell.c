#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>		/* O_RDONLY, O_WRONLY, O_CREAT, O_RDWR* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LER 0
#define ESCREVER 1
#define MAX 128

int comandoPipe;
int statsPipe;
int saldoR,saldoP;
int filhoExiste=1; //para quando o filho morrer para de ler as estaticias
int idstats;
char test[10000000];
int logE;


void lerPipe (char nome[], char comando[]) {
	char buf[2], novo[MAX];
	char *mat[2];
	int i=0;
	int fim=0;
	buf[1]=novo[0]='\0';

	while (!fim && read(comandoPipe,buf,1)>0) {
    	
    	if (buf[0]==';') {
    		fim=1;
    	}
    	else {
			if(buf[0]!=':'){
        	    strcat(novo,buf);
         	}else{
            	mat[i++] = strdup(novo);
            	novo[0]='\0';
        	}
		}
    	mat[i]=strdup(novo);

	}
	nome=strcpy(nome,mat[0]);
	comando=strcpy(comando,mat[1]);
	//printf("mat:--%s--%s\n",mat[0],mat[1]);
	//printf("%s\n",comando);
}





void initCmd(char* cmd[],int size){
    int i;
    for(i=0;i<size;i++){
        cmd[i]=NULL;
    }
}





int popenNew (char *comando[]) {
	int fds[2];
	int resfork;
	pipe(fds);
	
	resfork=fork();
	if (resfork<0) return -1;
	if (!resfork) { //filho
		dup2(fds[1],1);
		execvp(comando[0],comando);
	}
	else {
		wait(0L);
	}
	close(fds[0]);
	return fds[1];
}


void sigAcabar(int sig){
    filhoExiste=0;
}

void sigFim(int sig){
	write(logE,"Fecho do Servidor\n", strlen("Fecho do Servidor\n"));
	close(logE);
    kill(idstats,SIGINT);
    execlp("rm","rm","/tmp/comando","/tmp/estatistica","/tmp/saldoP","/tmp/saldoR",NULL);
    
}

void medeStats (int pid, char nome[]) {
   	char *comando[4], pid_str[10], buf[10], envioStats[MAX];
   	int respopen=-1;


   	comando[0]=strdup("./cpugasto");
   	sprintf(pid_str,"%d",pid);
   	comando[1]=strdup(pid_str);
   	comando[2]=strdup("1");
   	comando[3]=NULL;

    signal(SIGCHLD,sigAcabar);
    filhoExiste=1;
    while (filhoExiste) {

    	respopen=popenNew(comando);
    	int resread=read(respopen,buf,10);
    	if(resread<0){
    		write(logE,"Erro ao medir estatisticas, enviado o default\n",strlen("Erro ao medir estatisticas, enviado o default\n"));
    		sprintf(buf,"2.06");
    		resread=strlen(buf);
    	}
    	buf[resread]='\0';
    	close(respopen);
    	sprintf(envioStats,"%s:%s;",nome,buf);
    	write(statsPipe,envioStats,strlen(envioStats));
    }
}




void exeCmd(char nome[], char** cmd){
    int res, i=0;
    if(!(res=fork())) {
    	char tmp[MAX];
    	strcpy(tmp,"/tmp/"); strcat(tmp,nome);
    	
    	//printf("%s\n",tmp);

    	int k = open(tmp,O_RDWR);
    	//printf("%s_%d\n",tmp,k);


    	if (k<0) {
    		write(logE,"Erro comunicaçao com cliente\n",strlen("Erro comunicaçao com cliente\n"));
    		exit(-1);
    	}

    	dup2(k,1);
		dup2(k,2);    	

		int forkstats;

		if ((forkstats=fork())<0) {
			printf("ERRO\n");
			close(k);
			exit(-1);
		}

		if (forkstats) {	// é o pai
			medeStats(forkstats,nome);
			exit(-1);
		}
		else {
    		int resexec=execvp(cmd[0],cmd);

 			if (resexec<0) {
 				printf("ERRO ao executar comando\n");
 			}
 			exit(-1);
		}

    	close(k);
    	exit(-1);
    }

}


int parse (char process[], char *ret[]) {
	int i, j, k, sai=0;
	char buf[MAX];

	for (i=0, k=0; process[i]!='\0' && process[i]!='\n' && !sai; i+=j+1, k++) {		
		for (j=i; process[j]!='\0' && process[j]!=' '; j++);
		if (process[j]=='\0') sai=1;
		j=j-i;
		strncpy(buf,process+i,j);
		buf[j]='\0';
		ret[k]=strdup(buf);
	}
	return i;
}


void executaComando (char nome[], char comando[]){
	char* cmd[MAX];
	int i=0;

	initCmd(cmd,MAX);
	parse(comando,cmd);
	exeCmd(nome,cmd);
}


int temCreditos (char nome[]) {
	char nomev[MAX+1];
	char resp[3];
	sprintf(nomev,"%s;",nome);
	write(saldoP,nomev,strlen(nomev));
	kill(idstats,SIGUSR1);
	read(saldoR,resp,strlen("1;"));
	if(!strcmp(resp,"0;")) return 0;

	return 1;
}

int getStatID(){
	char buf[2];
	char id[6];
	buf[1]='\0';
	id[0]='\0';
	int fim=0;

	while (!fim && read(saldoR,buf,1)>0){
		if(buf[0]!=';'){
			strcat(id,buf);

		}else{
			fim=1;
		}

	}
	return atoi(id);
}
int main () {
    signal(SIGINT,sigFim);
	char nome[MAX], comando[MAX];
	if((logE = open("/tmp/CloudShell.log",O_CREAT | O_TRUNC | O_WRONLY,0777))<0){
		exit(-1);
	}
	dup2(logE,1);
	dup2(logE,2);
	close(0);
	write(logE,"Start Server\n",strlen("Start Server\n"));
	mkfifo("/tmp/comando",0666);
	mkfifo("/tmp/saldoP",0666);
	mkfifo("/tmp/saldoR",0666);

	comandoPipe=open("/tmp/comando",O_RDWR);
	saldoP= open("/tmp/saldoP",O_RDWR);
	saldoR=open("/tmp/saldoR",O_RDWR); 

	if (comandoPipe<0) {
		write(logE,"Canal de comandos indisponível\n",strlen("Canal de comandos indisponível\n"));
		exit(-1);
	}

	mkfifo("/tmp/estatistica",0666); //criar canal comunicacao com stats

	statsPipe=open("/tmp/estatistica",O_RDWR);

	if (statsPipe<0) {
		write(logE,"Canal de estatísticas indisponível\n",strlen("Canal de estatísticas indisponível\n"));
		exit(-1);
	}
	idstats = getStatID();

	while (1) {
		lerPipe(nome,comando);
		if (temCreditos(nome)==1) {
			executaComando(nome,comando);
		}
		else {
			
			executaComando(nome,"echo exit");
		}
	}
	return 0;
}
