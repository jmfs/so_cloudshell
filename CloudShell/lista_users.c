#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>		/* O_RDONLY, O_WRONLY, O_CREAT, O_RDWR* */
#include "lista_users.h"

#define MAX 20

typedef struct user {
	char nome[20];
	int pid;
	float saldo;
}user;

typedef struct list {
	User user;
	struct list *next;	
}list;


char* getNome (User u) {
	return u->nome;
}

int getPid (User u) {
	return u->pid;
}

float getSaldo (User u) {
	return u->saldo;
}

void setNome (User u, char *nome) {
	strcpy(u->nome,nome);
}

void setPid (User u, int pid) {
	u->pid=pid;
}

void setSaldo (User u, float saldo) {
	u->saldo=saldo;
}

void addSaldo (User u, float x) {
	u->saldo+=x;
}

void tiraSaldo (User u, float x) {
	u->saldo-=x;
}

char* userToString (User u) {
	char *s = malloc(100);
	sprintf(s,"Nome: %s\nPID: %d\nSaldo: %f",u->nome,u->pid,u->saldo);
	return s;
}


List addUser (List l, char *nome, int pid, float saldo) {
	List new;
	User u = searchUser(l,nome);
	if (u!=NULL) {
		u->pid = pid;
		u->saldo+=saldo;
		return l;

	}
	else {
		u=(User)malloc(sizeof(struct user));		
		strcpy(u->nome,nome);
		u->pid=pid;
		u->saldo=saldo;
		new=(List)malloc(sizeof(struct list));
		new->user=u;
		new->next=l;
		return new;
	}
}

int existeUser (List l, char *nome) {
	List aux;
	for (aux=l; aux!=NULL; aux=aux->next)
		if (strcmp(aux->user->nome,nome)==0) return 1;
	return 0;	
}


List deleteUser (List l, char *nome) {
	List aux;
	if (strcmp(nome,l->user->nome)==0) {
		free(l->user);
		l=l->next;
		return l;
	}
	for (aux=l; aux->next!=NULL; aux=aux->next) {
		if (strcmp(nome,aux->next->user->nome)==0) {
			free(aux->next->user);
			free(aux->next);
			aux->next=aux->next->next;
			return l;
		}
	}
	return l;
}


User getUser (List l, int index) {
	List aux;
	int i;
	for (i=0, aux=l; i<index && aux!=NULL; i++, aux=aux->next);
	if (i==index) return aux->user;
	else return NULL;
}


int indexOf (List l, char *nome) {
	List aux;
	int i;
	for (i=0, aux=l; aux!=NULL; i++, aux=aux->next)
		if (strcmp(nome,aux->user->nome)==0) return i;
	return -1;
}


int length (List l) {
	List aux;
	int i;
	for (i=0, aux=l; aux!=NULL; i++, aux=aux->next);
	return i;
}


void saveList (List l) {
	List aux;
	char buf[128];
	
	int fp = open("users.ini",O_CREAT | O_WRONLY | O_TRUNC, 0666);
	
	for (aux=l; aux!=NULL; aux=aux->next) {
	    sprintf(buf,"%s_%f\n",aux->user->nome,aux->user->saldo);
	    write(fp,buf,strlen(buf));
	}
	close(fp);
}


List loadList () {
    List l = NULL;
    char buf[2], linha[128];
    int fp = open("users.ini",O_RDONLY);
    
    if (fp<0) {
       return NULL;
    }
    
    int i, r;
    for (i=0; (r=read(fp,buf,1))>0; ) {
        linha[i]=buf[0];
        i++;
        if (buf[0]=='\n') {
            char nome[128], aux[256], saldobuf[128];
            float saldo;
            int passou=0; int k=0; int j;
            
            linha[i-1]='\0';
            
            
            sscanf(linha,"%s",aux);
            for (j=0; aux[j]!='\0'; j++) {
                if (aux[j]=='_') {
                    passou=1;
                    k=0;
                    nome[j]='\0';
                }
                else {
                    if (!passou) nome[j]=aux[j];
                    else saldobuf[k++]=aux[j];
                }
            }    
            saldobuf[k]='\0';
            
		    l=addUser(l,nome,-1,atof(saldobuf));	
            i=0;
        }
    }
    close(fp);
	return l;
}


User searchUser (List l, char *nome) {
	List aux;
	for (aux=l; aux!=NULL; aux=aux->next)
		if (strcmp(nome,aux->user->nome)==0) return aux->user;
	return NULL;
}
