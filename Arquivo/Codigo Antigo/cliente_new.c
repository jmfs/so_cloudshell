#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LER 0
#define ESCREVER 1
#define MAX 128


int main () {
	char nome[MAX], comando[MAX];
	int scomando, pipeR, lidos,fim;

	if((scomando = open("/tmp/comando",O_WRONLY))<0 ){
        printf("%d\n",scomando);
        printf("Servidor não disponível\n");
        exit(-1);
    }
    printf("Diga o nome de utilizador\n");
    scanf(" %s",nome);
    
    char tmp[MAX];
    strcpy(tmp,"/tmp/"); strcat(tmp,nome);
/*
    if (mkfifo(tmp,0666)<0) {
    	close(scomando);
    	printf("Não foi possivel estabelecer ligação para leitura do servidor\n");
    }*/
    mkfifo(tmp,0666);

	if ((pipeR=open(tmp,O_RDWR)) < 0) {
		close(scomando);
		printf("Erro na abertura do pipe\n");
	}

	do {
		printf("Introduza o comando:\n");
		scanf(" %99[^\n]",comando);
		if (strcmp(comando,"exit")) {
			char buf[2*MAX], bufR[MAX];
			strcpy(buf,nome); strcat(buf,":"); strcat(buf,comando); strcat(buf,";");
			//printf("%s\n",buf);
			int w=write(scomando,buf,strlen(buf));
			//printf("mandei %s %d\n",buf,w);
			fim=0;
			while (!fim && (lidos=read(pipeR,bufR,MAX))>0) {
				write(1,bufR,lidos);
				if(lidos<MAX) fim=1;
			}
		} 
	}while(strcmp(comando,"exit"));

	close(scomando);
	close(pipeR);

	printf("Adeus\n");

	return 0;
}