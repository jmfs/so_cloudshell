#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "lista_users.h"

#define LER 0
#define ESCREVER 1

List users=NULL;
int totUsers=0;
int fifo;
int readF;
int writeF;
int pidC;



void sigquit_filho (int sig) {
  close(readF);
  close(writeF);
  kill(pidC,SIGINT);
  //ver sina de moret de um filho
  exit(-1);
}



void sigoutFilho(int sig){
    totUsers--;
}

void sigal(int sig){
    printf("%d\n",fifo);
    alarm(1);
}

void lerComando(char* cmds[20]){
    int n=1;
    char buf[2];
    char cmd[20];
    buf[1]='\0';
    cmd[0]='\0';
    int i=0;
    while(n){
        n=read(0,buf,1);
        if(buf[0]!=' '){
            strcat(cmd,buf);
                    printf("%s\n",cmd);
        }
        else {
            cmds[i]=strdup(cmd);
            cmd[0]='\0';
            i++;
        }


    }
}

void initCmd(char* cmd[],int size){
    int i;
    for(i=0;i<size;i++){
        cmd[i]=NULL;
    }
}

void exeCmd(char** cmd){
    int res;
    if(!(res=fork())) execvp(cmd[0],cmd);
    else wait(0L);

}

void criaFilho(char nome[21],char pid[6]){
    int forkRes;
    int pidC = atoi(pid);
    User utl=searchUser(users,nome);

    if((forkRes=fork())<0){
        kill(pidC,SIGINT);
    }

    if(fork){ //pai
        setPid(utl,forkRes);
    }else{
        signal(SIGQUIT,sigquit_filho);
        //int readF;
        //int writeF;
        char buf[100];
        char* cmd[20];
        close(fifo);

        char *auxr = strdup(pid); strcat(auxr,"r");
        char *auxw = strdup(pid); strcat(auxw,"w");


        readF = open(auxr,O_RDONLY);
        writeF = open(auxw,O_WRONLY);
        printf("%d %d\n",readF,writeF);

        if(readF<0 || writeF<0){
            if(readF>0) close(writeF);
            if(writeF>0) close(writeF);
            kill(pidC,SIGINT);
            exit(-1);
        }

        dup2(readF,0);
        //dup2(writeF,1);

        while(1){
            //ver se o 0 é a bash
            if(getppid()==0) kill(getpid(),SIGQUIT);
            initCmd(cmd,20);
            lerComando(cmd);
            exeCmd(cmd);
        }
        exit(-1);
    }
}



void lerUser (char nome[21], char pid[6]){
    char buf[2];
    char* mat[3];
    char tmp[21];
    tmp[0]='\0';
    buf[1]='\0';
    int i=0;

    while(read(fifo,buf,1)>0 && buf[0]!=';'){ 
        
         if(buf[0]!=':'){
            strcat(tmp,buf);
         }else{
            mat[i++] = strdup(tmp);
            tmp[0]='\0';
         }
    }
    mat[2]=strdup(tmp);
    users = addUser(users,mat[2],-1,atof(mat[1]));
    strncpy(nome,mat[2],21);
    strncpy(pid,mat[0],6);
    pidC=atoi(mat[0]);
    totUsers++;
    
}




int main(){
    signal(SIGCHLD,sigoutFilho);
    signal(SIGALRM,sigal);
    char buf[1];
    char lastName[21]; 
    int nRead;
    char pidClient[6];
    int aux;
    int i=0;

    mkfifo("fifo",0666);
    //printf("%d",mkfifo("fifo",0666));



    if((fifo = open("fifo",O_RDWR,0666))<0) {
        printf("Erro abrir pipe\n");
        exit(-1);
    }

    if(fifo>0){
        while(1){
            lerUser(lastName,pidClient); //o cliente so pode escrever 6
            criaFilho(lastName,pidClient);  
        }
    }
}