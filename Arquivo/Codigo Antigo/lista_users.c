#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lista_users.h"

#define MAX 20

typedef struct user {
	char nome[20];
	int pid;
	float saldo;
}user;

typedef struct list {
	User user;
	struct list *next;	
}list;


char* getNome (User u) {
	return u->nome;
}

int getPid (User u) {
	return u->pid;
}

float getSaldo (User u) {
	return u->saldo;
}

void setNome (User u, char *nome) {
	strcpy(u->nome,nome);
}

void setPid (User u, int pid) {
	u->pid=pid;
}

void setSaldo (User u, float saldo) {
	u->saldo=saldo;
}

void addSaldo (User u, float x) {
	u->saldo+=x;
}

void tiraSaldo (User u, float x) {
	u->saldo-=x;
}

char* userToString (User u) {
	char *s = malloc(100);
	sprintf(s,"Nome: %s\nPID: %d\nSaldo: %f",u->nome,u->pid,u->saldo);
	return s;
}


List addUser (List l, char *nome, int pid, float saldo) {
	List new;
	User u = searchUser(l,nome);
	if (u!=NULL) {
		u->pid = pid;
		u->saldo+=saldo;
		return l;

	}
	else {
		u=(User)malloc(sizeof(struct user));		
		strcpy(u->nome,nome);
		u->pid=pid;
		u->saldo=saldo;
		new=(List)malloc(sizeof(struct list));
		new->user=u;
		new->next=l;
		return new;
	}
}

int existeUser (List l, char *nome) {
	List aux;
	for (aux=l; aux!=NULL; aux=aux->next)
		if (strcmp(aux->user->nome,nome)==0) return 1;
	return 0;	
}


List deleteUser (List l, char *nome) {
	List aux;
	if (strcmp(nome,l->user->nome)==0) {
		free(l->user);
		l=l->next;
		return l;
	}
	for (aux=l; aux->next!=NULL; aux=aux->next) {
		if (strcmp(nome,aux->next->user->nome)==0) {
			free(aux->next->user);
			free(aux->next);
			aux->next=aux->next->next;
			return l;
		}
	}
	return l;
}


User getUser (List l, int index) {
	List aux;
	int i;
	for (i=0, aux=l; i<index && aux!=NULL; i++, aux=aux->next);
	if (i==index) return aux->user;
	else return NULL;
}


int indexOf (List l, char *nome) {
	List aux;
	int i;
	for (i=0, aux=l; aux!=NULL; i++, aux=aux->next)
		if (strcmp(nome,aux->user->nome)==0) return i;
	return -1;
}


int length (List l) {
	List aux;
	int i;
	for (i=0, aux=l; aux!=NULL; i++, aux=aux->next);
	return i;
}


void saveList (List l) {
	List aux;
	FILE *fp = fopen("users.ini","w");
	for (aux=l; aux!=NULL; aux=aux->next)
		fprintf(fp,"%s %f\n",aux->user->nome,aux->user->saldo);
}


List loadList () {
	List l = NULL;
	char nome[MAX];
	float saldo;
	FILE *fp = fopen("users.ini","r");
	while (!feof(fp)) {
		fscanf(fp,"%s %f",nome,&saldo);
		l=addUser(l,nome,-1,saldo);	
	}
	return l;
}


User searchUser (List l, char *nome) {
	List aux;
	for (aux=l; aux!=NULL; aux=aux->next)
		if (strcmp(nome,aux->user->nome)==0) return aux->user;
	return NULL;
}