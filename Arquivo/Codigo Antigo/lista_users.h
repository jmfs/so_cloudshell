typedef struct user* User;
typedef struct list* List;

/* Devolve o nome de um utilizador */
char* getNome (User u);
/* Devolve o processID de um utilizador*/
int getPid (User u);
/* Devolve o saldo de um utilizador */
float getSaldo (User u);

/* Altera o nome de um utilizador */
void setNome (User u, char *nome);
/* Altera o processID de um utilizador */
void setPid (User u, int pid);
/* Altera o saldo de um utilizador */
void setSaldo (User u, float saldo);
/* Adiciona x ao saldo de um utilizador */
void addSaldo (User u, float x);
/* Retira x ao saldo de um utilizador */
void tiraSaldo (User u, float x);

/**
 * Adiciona um utilizador à lista
 * @param  l     Lista de utilizadores
 * @param  nome  Nome do utilizador
 * @param  pid   ProcessID do utilizador
 * @param  saldo Saldo inicial do utilizador
 * @return       Lista de utilizadores actualizada
 */
List addUser (List l, char *nome, int pid, float saldo);

/**
 * Verifica se um utilizador existe na lista
 * @param  l    Lista de utilizadores
 * @param  nome Nome do utilizador que se pretende procurar
 * @return      1 caso o utilizador exista, 0 caso contrário
 */
int existeUser (List l, char *nome);

/**
 * Remove um utilizador da lista
 * @param  l    Lista de utilizadores
 * @param  nome Nome do utilizador que se pretende remover
 * @return      Lista de utilizadores actualizada
 */
List deleteUser (List l, char *nome);

/**
 * Devolve o utilizador que se encontra numa determinada posição
 * @param  l     Lista de utilizadores
 * @param  index Índice
 * @return       Utilizador encontrado, NULL caso não exista
 */
User getUser (List l, int index);

/**
 * Devolve o índice de um utilizador na lista
 * @param  l    Lista de utilizadores
 * @param  nome Nome do utilizador
 * @return      Índice onde o utilizador se encontra,
 *              -1 caso não exista
 */
int indexOf (List l, char *nome);

/* Devolve o tamanho da lista */
int length (List l);

/**
 * Lê a lista guardada no ficheiro 'users.ini'
 * @return Lista de utilizadores lida
 */
List loadList ();

/**
 * Guarda uma lista num ficheiro 'users.ini' com o seguinte formato:
 * [Nome] [Saldo]
 * @param l Lista que se prentede guarda
 */
void saveList (List l);

/**
 * Procura por um utilizador na lita
 * @param  l    Lista de utilizadores
 * @param  nome Nome do utilizador que se pretende pesquisar
 * @return      Utilizador encontrado, NULL caso não exista
 */
User searchUser (List l, char *nome);