#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LER 0
#define ESCREVER 1


typedef void (*sighandler_t)(int);

int dedicEscr,dedicLeit; 


void sigquit_handler (int sig) {
  close(dedicEscr);
  close(dedicLeit);
  printf("Saldo esgotado.\n");
  exit(-1);
}


void sigint_handler (int signal){
    close(dedicEscr);
    close(dedicLeit);
    printf("Servidor Cheio.\n");
    exit(-1);
}


int executaComandos(){
    int rd,print;
    char buff[10];
    printf("%d %d\n",dedicEscr,dedicLeit);
    while((rd = read(0,buff,10))>0){
        write(dedicLeit,buff,rd);
        write(1,buff,rd);
        printf("%s\n",buff);
    }
    write(dedicLeit,";",rd);
    if(!strcmp("exit",buff)) return 0;
    while(print=read(dedicEscr,buff,10)){
        write(1,buff,print);
    }
    // verse nao nao é presiso brir de novo o pipe
}


int main(){
    signal(SIGQUIT,sigquit_handler);
    signal(SIGINT,sigint_handler);
    int nRead;
    char nome[21];
    float saldo;
    int pid = getpid();
    int tam;
    int servercom;
    char carr[5];
    char id[6];
    char tofifo[50];
    int contin;


    if((servercom = open("fifo",O_WRONLY))<0 ){
        printf("%d\n",servercom);
        printf("Servidor não disponível\n");
        exit(-1);
    }
    else{
        printf("ola\n");
    }


// user nao pode por :; no nome
    printf("Diga o nome de utilizador\n");
    scanf(" %s",nome);
    printf("%s\n",nome);
    /*tam = read(0,nome,20);
    nome[tam] ='\0';*/

    do{
        printf("Diga o saldo a carregar\n");
        scanf("%f",&saldo);
    }while(saldo<50 || saldo >300);


    sprintf(carr,"%f",saldo);
    sprintf(id,"%d",pid);
    sprintf(tofifo,"%d:%f:%s;",pid,saldo,nome);
    /*
    strcat(tofifo,id);
    strcat(tofifo,":");
    strcat(tofifo,carr);
    strcat(tofifo,":");
    strcat(tofifo,nome);
    strcat(tofifo,";");*/


    char *auxr = strdup(id); strcat(auxr,"r");
    char *auxw = strdup(id); strcat(auxw,"w");

    printf("antes do open\n");

    //serividor escreve para este e o vlinete le
    mkfifo(auxw,0666);

    printf("1º makefifo\n");
    dedicLeit = open(auxr,O_RDWR);

    //cria canais de comunicaçao com a maquina remota
    //serividor le deste e o vlinete escreve
    mkfifo(auxr,0666);

    printf("makefifo\n");
    dedicEscr = open(auxw,O_RDWR);


    printf("%d %d\n",dedicLeit,dedicEscr);

    //diz ao servidor que quer comunicar
    write(servercom,tofifo,strlen(tofifo));
    close(servercom);
    contin=1;
    while(contin){

        contin=executaComandos();
    }
    printf("Logout\n");
    close(dedicLeit);
    close(dedicEscr);
    return 0;
}