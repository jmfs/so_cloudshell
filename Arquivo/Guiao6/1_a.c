/* Criar um pipe com nome */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main () {
  /* Criar o pipe com nome */
  mkfifo("/tmp/fifo", 0666 /* S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH*/);
  /* Desassociar o programa */
  unlink("fifo");

  return 0;
}
