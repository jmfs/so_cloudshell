 /* Escreva um programa cliente que envia para o servidor o seu argumento. 
  * Cliente e servidor devem comunicar via pipes com nome. */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h> 

#define LINE 128  // Tamanho da linha de input

int main () {
  char buffer[LINE]; // Para guardar as mensagens
  int fd, nbytes;   // File descriptor para o pipe, n de bytes lidos

  /* Abrir o pipe */ 
  fd = open("/tmp/ch", O_WRONLY);

  if (fd == -1) { printf("Error!\n"); return 0; }

  while ((nbytes = read(0, buffer, LINE)) != 0) {
    write(fd, buffer, nbytes);
  }

  close(fd); 
  unlink("/tmp/ch");
}
