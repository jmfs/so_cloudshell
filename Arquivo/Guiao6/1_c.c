/* Abre o pipe e escreve para o std output tudo o que 
 * leu do pipe */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#define MAXLINE 128

int main () {
  int fd, bytes;
  char buff[MAXLINE];
  fd = open("/tmp/fifo", O_RDONLY); /* Criar descriptor de leitura para o named pipe */

  if (fd == -1) {
    printf("Erro a abrir o pipe\n");
    return 0;
  }

  while ( (bytes = read(fd, buff, MAXLINE)) != 0 ) {
    write(1, buff, bytes);
  }
  
  close(fd);
  unlink("fifo");

  return 0;
}
