/* Abre o pipe e tudo o que ler do standard input 
 * escreve para o pipe, este programa deve ser 
 * corrido junt com o ./1_c, noutra janela, 
 * para poder ver o resultado */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#define MAXLINE 128

int main () {
  int fd, bytes;
  char buff[MAXLINE];

  fd = open("/tmp/fifo", O_WRONLY); /* Abrir o named pipe só para escrita */

  if (fd == -1) {
    printf("Erro a abrir o pipe\n");
    return 0;
  }  
  
  while ( (bytes = read(0, buff, MAXLINE)) ) { 
    write(fd, buff, bytes);
  }

  close(fd);    /* Fechar o descriptor para o pipe */
  unlink("fifo"); /* Desassociar do pipe */

  return 0;
}
