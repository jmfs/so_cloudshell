/* Escreva um programa “servidor”, que fique a correr em background, e acrescente a 
 * um ficheiro de “log” todas as mensagens que sejam enviadas por “clientes”.
 * Cliente e Servidor devem comunicar via pipes com nome. */

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h> 

#define LINE 128          // Tamanho da linha de input

int main () {
  char buffer[128];       // Para guardar as mensagens
  int fd, log, nbytes;    // File descriptor para o pipe, n de bytes lidos

  /* Criar o pipe para comunicação */
  mkfifo("/tmp/ch", S_IWUSR | S_IRUSR | S_IRGRP | S_IROTH);

  /* Criar o ficheiro de log */
  log = open("/tmp/log", O_WRONLY | O_CREAT, 0666);
  /* Abrir pipe para leitura */
  fd = open("/tmp/ch", O_RDONLY);

  /* Colocar o servidor em loop, para terminar Ctrl-C */
  while (1) {
    nbytes = read(fd, buffer, LINE);
    write(log, buffer, nbytes);
  }

  close(fd);
  close(log);
  unlink("/tmp/ch");
}
