#include <stdlib.h>
#include <unistd.h>   /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */

int main (int argc, char * argv[])
{
  if ( argc == 2 )
  {
    // Criar ficheiro se não existir, apenas para escrever no ficheiro
    int fd = open(argv[1], O_CREAT | O_WRONLY | O_TRUNC, 0666);
    int size = 0;
    char a = 'a';

    for (size = 0; size <= 1024 * 1024; size++)
      write(fd, &a, 1);

    close(fd);
  } else {
    write(1, "Wrong usage\n", 13);
    exit(1);
  }
  return 0;
}
