/* Verifica se dois ficheiros são iguais byte a byte */

#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define MAXLINE 128

size_t readln (int fildes, char * buf, size_t nbyte) {
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}

int main (int argc, char ** argv) {
  char buff1[128], buff2[128];  /* Para guardar o input dos dois ficheiros */
  int fd1, fd2;                 /* Descriptores de ficheiro */
  int linha = 0;                /* Número da linha */
  int nb1, nb2;                 /* Número de bytes */
  int i, j;                     /* Iteradores auxiliares */

  if (argc != 3) {
    printf("Devem ser fornecidos dois ficheiros apenas!\n");
    return 1;
  }

  /* Abrir os ficheiros */
  fd1 = open(argv[1], O_RDONLY);
  fd2 = open(argv[2], O_RDONLY);

  /* Verificar se os ficheiros existem */
  if (fd1 == -1) {
    printf("O ficheiro '%s' não existe!\n", argv[1]);
    return 1;
  }

  if (fd2 == -1) {
    printf("O ficheiro '%s' não existe!\n", argv[2]);
    return 1;
  }

  do {
    linha++;
    nb1 = readln(fd1, buff1, MAXLINE);  /* Ler linha primeiro ficheiro */
    nb2 = readln(fd2, buff2, MAXLINE);  /* Ler linha segundo ficheiro */

    for (i = 0; i <= nb1; i++)
      if (buff1[i] != buff2[i]) {
        printf("Files differ: Line %d Byte %d\n", linha, i + 1);
        return 1;
      }
  } while ((nb1 != 0) && (nb2 != 0) && (nb1 == nb2));

  /* Tamanho das linhas é diferente */
  if ((nb1 != 0) && (nb2 != 0)) {
    for (i = 0; (i <= nb1) && (i <= nb2); i++)
      if (buff1[i] != buff2[i]) {
        printf("Files differ at line %d, byte %d", linha, i + 1);
        return 1;
      }
  }

  printf("Files are equal!\n");
  return 0;
}
