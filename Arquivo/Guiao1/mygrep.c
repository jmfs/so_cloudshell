#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define MAXLINE 128

size_t readln (int fildes, char * buf, size_t nbyte) {
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}

/* Função para verificar se str1 está contida na str2 */ 
int subStr (char * str1, char * str2) {
  int i = 0;

  while ((str1[i] != '\0') && (str2[i] != '\0') && (str1[i] == str2[i]))
      i++;
  if (str1[i] == '\0') return 1;
  else return 0;
}

int main (int argc, char ** argv) {
  char buff[MAXLINE];
  char * pattern = argv[1];
  int i, j, fd, nbytes;


  for (i = 2; i < argc; i++) {
    fd = open(argv[i], O_RDONLY);

    if (fd == -1) { /* File does not exist */
      printf("%s File does not exist!\n", argv[i]);
    } else {        /* File exists */
      while ( (nbytes = readln(fd, buff, MAXLINE)) != 0 ) {
          for (j = 0; j < nbytes; j++)
            if ( subStr(pattern, &(buff[j])) == 1 ) {
              write(1, buff, nbytes);
              break;
            }
      }
    } 
  } 
}

