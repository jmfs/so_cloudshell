#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define MAXLINE 128

size_t readln (int fildes, char * buf, size_t nbyte) {
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}

int main (int argc, char ** argv) {
  char buff[MAXLINE];
  int lines = 10, bytes = 1, fd, i;

  fd = open(argv[1], O_RDONLY);

  if (fd == -1) {
    printf("File doesn't exist!");
    return 1;
  }

  /* Number of lines was specified */
  if (argc == 3)
    lines = atoi((argv[2] + 1));

  for (i = 0; (i < lines) && (bytes != 0); i++) {
    bytes = readln(fd, buff, MAXLINE);
    write(1, buff, bytes);
  }

  return 0;
}
