#include <unistd.h>   /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */

int main (int argc, char * argv[])
{
  char c = 'x';

  while ( read(0, &c, 1) != 0 )
  {
    write(1, &c, 1);
  }

  return 0;
}
