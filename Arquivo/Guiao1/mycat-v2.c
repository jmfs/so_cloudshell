#include <stdlib.h>
#include <unistd.h>   /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */

int main (int argc, char * argv[])
{
  if ( argc != 2 )
  {
    write(1, "Argumentos inválidos\n", 22);
    exit(1);
  } else {
    int n = 0, i = 0, numread = 0;

    // Transformar o argumento em int
    for ( i = 0; argv[1][i] != '\0'; i++)
    {
      n = n * 10;
      n += argv[1][i] - '0';
    }

    char * buffer = (char *) malloc( n * sizeof(char) );

    while ( (numread = read(0, buffer, n)) )
    {
      write(1, buffer, numread);
    }
  }

  return 0;
}
