/* 6. Implemente, utilizando a função readln, um programa com funcionalidade similar ao comando nl, o
qual repete linha a linha o standard input ou o conteúdo de um (só) ficheiro especificado na sua linha de
comando. Cada linha repetida é numerada sequencialmente. */

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h> // Needed for O_CREAT...

ssize_t readln(int file, char *buf, size_t nbyte){
    int i = 0;
    int n; 
    do{
        n = read (file, &buf[i], 1);
        i++;
    }while (  n>0  && (buf[i-1] != '\n') && i<nbyte);
    buf[i]=0;
    if (n>0) return i;
    else return -1;
}

int main (int argc, char *argv[]){
    int i = 0;
    int file = open (argv[1], O_RDONLY);
    char buf[1000];
    if (argc == 2)
        while (readln(file,buf,1000) > 0) {
            printf("%d  %s", i, buf);
            i++;
        }
    else
         while (readln(0,buf,1000) > 0) {
            printf("%d.s  %s", i, buf);
            i++;
        }
    close(file);

}