#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>   /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>    /* O_RDONLY, O_WRONLY, O_CREAT, O_* */

// Protótipo readln
size_t readln (int , char * , size_t );
int numberOfDigits (int);

int main ()
{
  char buf[1024];
  char output[1026];
  int i = 1, bytesread;

  // Ler linha a linha e imprimir o número da linha
  while ( (bytesread = readln(0, buf, 1024) ) != 0 )
  {
    // Guardar a linha já com o número em nova string
    sprintf(output, "%d %s", i, buf);
    // Só funciona até linhas decimais por causa do +3

    write(1, output, bytesread + numberOfDigits(i) + 1);
    i++;
  }
  write(1, "\n", 2);
  return 0;
}

size_t readln (int fildes, char * buf, size_t nbyte)
{
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}

int numberOfDigits (int n)
{
  int count=0;

  while(n!=0) { 
    n/=10; /* n=n/10 */ 
    ++count;
  }
  return count;
}
