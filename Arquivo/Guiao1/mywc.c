#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

#define MAXLINE 128

size_t readln (int fildes, char * buf, size_t nbyte) {
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}

int main (int argc, char ** argv) {
  char buff[MAXLINE]; /* Para guardar o input */
  int nbytes;         /* Num. de bytes lidos */
  int fd;             /* Descritor de ficheiros */
  int i, j;           /* Iteradores */
  int w, l, b;     /* Palavras, linhas e bytes */
  
  /* Inicializar contadores */
  w = l = b = 0;

  if (argc > 1) {
    for (i = 1; i < argc; i++) {
      fd = open(argv[i], O_RDONLY);  /* Abrir para leitura */

      if (fd == -1)
        printf("%s não existe!", argv[i]);
      else {
        /* Ler ficheiro linha a linha */
        while ((nbytes = readln(fd, buff, MAXLINE)) != 0) {
          l++;          /* Incrementar linha */
          b += nbytes;  /* Incrementar bytes */

          /* Contar numero de palavras */
          for (j = 1; j <= nbytes; j++) {
            if (buff[j] == ' ' && buff[j-1] != ' ')
              w++;
            if (buff[j] == '\0' && buff[j-1] != ' ')
              w++;
          }
        }
      }

      /* Inprimir resultados */
      printf("%5d %5d %5d\n", l, w, b);
    }
  }
}
