#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main ()
{
  int fpids[10];
  int status, fpid = 1, i = 0;

  while ( i != 10 && fpid ) {
    fpid = fork();
    // Pai
    if (fpid) { fpids[i] = fpid; i++; }
  }

  // Filho
  if (!fpid) { printf("Olá eu sou %d , o meu pai é %d.\n", getpid(), getppid()); _exit(4); }

  if (fpid) {
    i = 0;
    while ( i != 10 )
    {
      waitpid(fpids[i], &status, 0);
      i++;
    }
  }

  return 0;
}
