#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

main()
{
  int status;
  int fpid = fork();

  if ( fpid == 0 )
  {
    printf("Filho\n");
    _exit(4);
  } else {
    printf("Pai teve filho %d\n", fpid);
    wait(&status);
    printf("Filho morreu com %d\n", WEXITSTATUS(status));
  }
  printf("Ambos\n");
}
