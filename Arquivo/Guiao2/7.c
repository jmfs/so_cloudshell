#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#define L 5
#define C 25

int main () {
  int fpids[5], fpid = 1;
  int status, i, j, found = 0;
  int mat[L][C];

  // Populate matrix with 0
  for (i = 0; i < L; i++)
    for (j = 0; j < C; j++)
      mat[i][j] = 0;

  i = 0;

  mat[2][3] = 1;
  mat[4][10] = 1;

  while (i != 5 && fpid != 0) {
    fpid = fork();
    if (fpid != 0) { fpids[i] = fpid; i++; } 
  }

  if ( fpid == 0 )      /* Pai */
  {
    found = 0;
    for (j = 0; j < C; j++)
      if (mat[i][j]) found = 1;

    _exit(found);
  }

  if (fpid != 0) {      /* Filho */
    for (i = 0; i != 5; i++) {
      waitpid(fpids[i], &status, 0);
      if (WEXITSTATUS(status) == 1) printf("Linha nº %d tem um 1.\n", i);
    }
  }

  return 0;
}
