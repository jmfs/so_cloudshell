#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main ()
{
  int fpid, status, i = 0;

  while ( i != 10 ) {
    fpid = fork();

    if (fpid == 0) { /* Filho */
      printf("FILHO -> %d || PAI -> %d\n", getpid(), getppid());
      printf("--------------------------\n");
    } else {        /* Pai */
      wait(&status);  /* Esperar pelo termino do filho */
      _exit(4);       /* Terminar */
    }
    i++;
  }

  return 0;
}
