#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main ()
{
  int status;
  int fpid = fork();

  if ( fpid == 0 )
  {
    printf("Ola\n");
    printf("(Filho) Filho: %d\n", getpid() );
    printf("(Filho) Pai: %d\n", getppid() );
    _exit(4);
  }
  else
  {
    printf("(Pai) Pai do Pai: %d\n", getppid() );
    printf("(Pai) Pai: %d\n", getpid() );
    printf("(Pai) Filho: %d\n", fpid);
    wait(&status);
  }

  return 0;
}
