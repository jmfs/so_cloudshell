#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#define L 5
#define C 25

int main () {
  int fpid, status, i, j;
  int mat[L][C];

  for (i = 0; i < L; i++)
    for (j = 0; j < C; j++)
      mat[i][j] = 0;

  mat[3][10] = 1;

  i = 0;

  while (i != L) {
    for (j = 0; j < C; j++)
      if (mat[i][j]) printf("Eu, %d, encontrei o 1!\n", getpid());

    fpid = fork();

    if (fpid == 0) i++; /* Filho */
    else {              /* Pai */
      wait(&status);
      _exit(4);
    }
  }

  return 0;
}
