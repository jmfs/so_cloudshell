#include <unistd.h>
#include <stdio.h>
#include <string.h>

int main (int argc, char ** argv) {
  if (argc < 2) {
    printf("Pelo menos 2 argumentos devem ser passados!");
    return 1;
  }

  execvp(argv[1], &argv[1]);
  return 0;
}
