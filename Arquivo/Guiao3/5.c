#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>

/**
 * Implemente um programa que execute concorrentemente uma lista de executa ́veis 
 * especificados como argumentos da linha de comando. Considere os executa ́veis sem 
 * quaisquer argumentos pro ́prios. O programa devera ́ esperar pelo fim da execuc ̧a ̃o 
 * de todos processos por si criados.
 */

int main (int argc, char * argv[])
{
  int fpids[argc-1];
  int fpid = 1, i = 1, status;

  while ((fpid != 0) && (i < argc)) { /* Criar os filhos */
    fpid = fork();
    if (fpid != 0) { fpids[i-1] = fpid; i++; }
  }

  if (fpid == 0) {                    /* Filhos executam os processos */
    execlp(argv[i], argv[i], NULL);
    _exit(4);
  } else {                            /* Pai esperam pelo termino dos filhos */
    for (i = 0; i < argc; i++)
      waitpid(fpids[i], &status, 0);
  }

  return 0;
}
