/* Processo cria um filho que executa o comando 'ls -l' */

#include <unistd.h>
#include <sys/wait.h>

int main () {

  if (fork() == 0)
    execlp("ls", "ls", "-l", NULL);
  else
    wait(NULL);

  return 0;
}
