#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

/* Executa concorrentemente um conjunto de programas passados como argumento, 
 * deverá rexecutar cada programa enquanto este não terminar com código de saída nulo.
 * Deverá ainda imprimir quantas vezes cada programa foi corrido */

int main (int argc, char ** argv) {
  int i = 0;
  int execs[10];
  int status;

  for (i = 0; i < 10; i++)
    execs[i] = 0;

  i = 0;
  while (i < (argc - 1)) {
    if (fork() == 0) {  /* Filho */
      execlp(argv[i+1], argv[i+1], NULL);
    } else {        /* Pai */
      status = 1;
      execs[i]++;
      wait(&status);
      if (status == 0) i++;
    }
  }

  for (i = 0; i < (argc - 1); i++)
    printf("%s -> %d\n", argv[i + 1], execs[i]);

  return 0;
}
