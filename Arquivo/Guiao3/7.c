#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

/*
 * Implemente um interpretador de comandos muito simples ainda que inspirado na bash. 
 * O interpretador deverá executar comandos especificados numa linha de texto introduzida 
 * pelo utilizador. Os comandos são compostos pelo nome do programa a executar e uma 
 * eventual lista de argumentos. Os comandos podem ainda executar em primeiro plano, ou 
 * em pano de fundo, caso o utilizador termine a linha com &. O interpretador deverá terminar 
 * a sua execução quando o utilizador invocar o comando interno exit ou quando assinalar 
 * o fim de ficheiro (Control-D no início de uma linha em sistemas baseados em Unix).
 */
size_t readln (int fildes, char * buf, size_t nbyte);

int main () {
  int i = 0, j = 0, y = 0, running = 1;
  char * args[16];     /* Save arguments */
  char input[1024];
  char * temp;

  while (running) {
    if (readln(0, input, 1024) == 0)  /* Verificar se Ctrl-D foi pressionado */
      running = 0;
    else {

      /* Separar os argumentos */
      temp = strtok(input, " \n");
      for (i = 0; temp != NULL; i++) {
        args[i] = temp;
        temp = strtok(NULL, " \n");
      }
      args[i] = NULL;

      if ( !strcmp(args[0], "exit") )   /* Sair do programa */
        running = 0;
      else                              /* Correr o processo */
        if (!fork())
          execvp(args[0], args);
        else
          if (strcmp(args[i-1], "&")) wait(NULL);
    }
  }
  
  return 0;
}

size_t readln (int fildes, char * buf, size_t nbyte)
{
  size_t bytesread = 0, i = 0;
  char c;

  do {
    i = 0;
    i = read(fildes, &c, 1);
    if (i != 0) { buf[bytesread] = c; bytesread++; }
  } while ( i != 0 && bytesread != nbyte && c != '\n');

  buf[bytesread] = '\0';

  // Devolver o numero de bytes lidos
  return bytesread;
}
