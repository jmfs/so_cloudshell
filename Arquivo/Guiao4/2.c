#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main ()
{
  int fd1, fd2, fd3, numread;
  char line[1024];

  fd1 = open("/etc/passwd", O_RDONLY , S_IRUSR | S_IWUSR);
  fd2 = open("saida.txt", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
  fd3 = open("erros.txt", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

  dup2(fd1, 0);
  dup2(fd2, 1);
  dup2(fd3, 2);

  close(fd1);
  close(fd2);
  close(fd3);

  if (!fork()) {
    while ( (numread = read(0, line, 256)) )
    {
      // printf escreve no standard output (1)
      printf("%s", line);
      write(2, line, numread);
    }
  }

  return 0;
}
