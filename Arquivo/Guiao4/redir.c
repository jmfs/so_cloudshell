/* Escreva um programa redir que permita executar um comando, opcionalmente redireccionando 
 * a entrada e/ou a saída. O programa poderá ser invocado, com:
 * redir [-i fich_entrada] [-o fich_saida] comando arg1 arg2 ...
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h> /* para usar strcmp() e strcpy() */

int main (int argc, char * argv[])
{
  int entrada, saida;               /* descritores de entrada e saida */
  char linha[1024];                  /* espaço para alocar a string lida */
  char comando[32];                  /* comando a ser corrido */
  strcpy(comando, "");

  /* atribuir os valores standard aos descritores */
  entrada = 0; saida = 1;

  ++argv;
  /* Verificar se há flags utilizadas */
  while ( *argv != NULL && !strcmp(comando,"") )
  {
    /* faltam verificações se os ficheiros são passados ou não
     * mas como é apenas para teste não e necessario, assumimos então que 
     * e sempre passado um nome de um ficheiro valido */

    if ( !strcmp(*argv, "-i") ) {       /* Alterar input */
      ++argv;
      entrada = open(*argv, O_RDONLY, S_IRUSR);

      if (entrada == -1) {              /* Ficheiro não existe */
        printf("O ficheiro '%s' não existe\n", *argv);
        entrada = 0;
      }
    }
    else if ( !strcmp(*argv, "-o") ) {  /* Alterar output */
      argv++;
      saida = open(*argv, O_WRONLY | O_APPEND , S_IWUSR);

      if (saida == -1) {                /* Ficheiro não existe */
        printf("O ficheiro '%s' não existe.", *argv);
        saida = 1;
      }
    }
    else strcpy(comando, *argv);

    argv++;
  }

  /* redirecionar descritores */
  dup2(entrada, 0);
  dup2(saida, 1);

  /* e necessario utilizar argv-- porque senão argv vai a null
   * e deve ter no minimo o nome do processo que esta a ser invocado */
  execvp(comando, (--argv)); 

}
