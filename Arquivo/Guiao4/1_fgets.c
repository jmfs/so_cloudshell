#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main () {
  FILE * f1, * f2, * f3;
  char line[1024];

  f1 = fopen("/etc/passwd", "r");
  f2 = fopen("saida.txt", "w+");
  f3 = fopen("erros.txt", "w+");

  while ( fgets(line, 1024, f1) != 0 ) {
    fprintf(f2, "%s", line);
    fprintf(f3, "%s", line);
  }
  return 0;
}

