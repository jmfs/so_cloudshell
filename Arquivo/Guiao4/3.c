/* Modifique novamente o programa inicial de modo a que seja executado o comando wc, 
 * sem argumentos, depois do redireccionamento dos descritores de entrada e saída. 
 * Note que, mais uma vez, as associac ̧o ̃es – e redireccionamentos – de descritores 
 * de ficheiros sa ̃o preservados pela primitiva exec().
 */

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

int main () {
  int fd1, fd2, fd3, numread;
  char line[1024];

  fd1 = open("/etc/passwd", O_RDONLY , S_IRUSR | S_IWUSR);
  fd2 = open("saida.txt", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
  fd3 = open("erros.txt", O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);

  /* Redirecionar descritores */
  dup2(fd1, 0); dup2(fd2, 1); dup2(fd3, 2);

  /* Fechar descritores desnecessarios */
  close(fd1); close(fd2); close(fd3);

  /* Executar o comando wc */
  execlp("wc", "wc", NULL);

  return 0;
}
