#include <unistd.h>     /* chamadas ao sistema: defs e decls essenciais */
#include <fcntl.h>		/* O_RDONLY, O_WRONLY, O_CREAT, O_RDWR* */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

#define LER 0
#define ESCREVER 1
#define MAX 128

int comandoPipe;
int statsPipe;
int saldoR,saldoP;
int filhoExiste=1; //para quando o filho morrer para de ler as estaticias
int idstats;
char test[10000000];


void lerPipe (char nome[], char comando[]) {
	char buf[2], novo[MAX];
	char *mat[2];
	int i=0;
	int fim=0;
	buf[1]=novo[0]='\0';

	while (!fim && read(comandoPipe,buf,1)>0) {
    	
    	if (buf[0]==';') {
    		fim=1;
    	}
    	else {
			if(buf[0]!=':'){
        	    strcat(novo,buf);
         	}else{
            	mat[i++] = strdup(novo);
            	novo[0]='\0';
        	}
		}
    	mat[i]=strdup(novo);

	}
	nome=strcpy(nome,mat[0]);
	comando=strcpy(comando,mat[1]);
}


void initCmd(char* cmd[],int size){
    int i;
    for(i=0;i<size;i++){
        cmd[i]=NULL;
    }
}


int popenNew (char *comando[], int fds[]) {
	int resfork;
	char buf[MAX];
	
	resfork=fork();
	if (resfork<0) return -1;
	if (!resfork) { //filho
		dup2(fds[1],1);
		execvp(comando[0],comando);
	}
	else {
		wait(0L);
	}

	close(fds[0]);
	return fds[1];
}


void sigAcabar(int sig){
    filhoExiste=0;
}


void medeStats (int pid, char nome[]) {
   	char *comando[4], pid_str[10], buf[10], envioStats[MAX];
	int respopen=-1;

	int fds[2];

   	char cmd[MAX];

   	comando[0]=strdup("./cpugasto");
   	sprintf(pid_str,"%d",pid);
   	comando[1]=strdup(pid_str);
	comando[2]=NULL;   	

    signal(SIGCHLD,sigAcabar);
    filhoExiste=1;
    while (filhoExiste) {
		pipe(fds);

		char aux[MAX];

    	respopen=popenNew(comando,fds);

    	int resread=read(fds[1],buf,10);
    	
    	if(resread<0){
    		sprintf(buf,"9.69");
    		resread=strlen(buf);
    	}
    	
    	buf[resread]='\0';

    	close(respopen);
    	sprintf(envioStats,"%s:%s;",nome,buf);

    	write(statsPipe,envioStats,strlen(envioStats));

   	close(fds[0]);
    }

}




void exeCmd(char nome[], char** cmd){
    int res, i=0;
    if(!(res=fork())) {

    	char tmp[MAX];
    	strcpy(tmp,"/tmp/"); strcat(tmp,nome);
    	
    	int k = open(tmp,O_RDWR);

    	if (k<0) {
    		printf("Erro comunicaçao com cliente\n");
    		exit(-1);
    	}


    	dup2(k,1);
		dup2(k,2);    	

		int forkstats;

		if ((forkstats=fork())<0) {
			printf("Erro\n");
			close(k);
			exit(-1);
		}

		if (forkstats) {	// é o pai
			medeStats(forkstats,nome);
			exit(-1);
		}
		else {
    		int resexec=execvp(cmd[0],cmd);

    		//caso o exec nao de
 			if (resexec<0) {
           		printf("ERRO");
 			}
 			exit(-1);
		}

    	close(k);
    	exit(-1);
    }

}


int parse (char process[], char *ret[]) {
	int i, j, k, sai=0;
	char buf[MAX];

	for (i=0, k=0; process[i]!='\0' && process[i]!='\n' && !sai; i+=j+1, k++) {		
		for (j=i; process[j]!='\0' && process[j]!=' '; j++);
		if (process[j]=='\0') sai=1;
		j=j-i;
		strncpy(buf,process+i,j);
		buf[j]='\0';
		ret[k]=strdup(buf);
	}
	return i;
}


void executaComando (char nome[], char comando[]){
	char* cmd[MAX];
	int i=0;

	initCmd(cmd,MAX);
	parse(comando,cmd);
	while (cmd[i]!=NULL) {
		i++;
	}
	exeCmd(nome,cmd);
}


int temCreditos (char nome[]) {
	char nomev[MAX+1];
	char resp[3];
	sprintf(nomev,"%s;",nome);
	write(saldoP,nomev,strlen(nomev));
	kill(idstats,SIGUSR1);
	read(saldoR,resp,2);
	resp[2]='\0';
	printf("resp:_%s\n",resp);
	if(!strcmp(resp,"0;")) return 0;

	return 1;
}

int getStatID(){
	char buf[2];
	char id[6];
	buf[1]='\0';
	id[0]='\0';
	int fim=0;

	while (!fim && read(saldoR,buf,1)>0){
		if(buf[0]!=';'){
			strcat(id,buf);

		}else{
			fim=1;
		}

	}
	return atoi(id);
}


int main () {

	char nome[MAX], comando[MAX];

	mkfifo("/tmp/comando",0666);
	mkfifo("/tmp/saldoP",0666);
	mkfifo("/tmp/saldoR",0666);

	comandoPipe=open("/tmp/comando",O_RDWR);
	saldoP= open("/tmp/saldoP",O_RDWR);
	saldoR=open("/tmp/saldoR",O_RDWR); 

	if (comandoPipe<0) {
		printf("Canal de comandos indisponível\n");
		exit(-1);
	}

	mkfifo("/tmp/estatistica",0666); //criar canal comunicacao com stats

	statsPipe=open("/tmp/estatistica",O_RDWR);

	if (statsPipe<0) {
		printf("Canal de estatísticas indisponível\n");
		exit(-1);
	}
	idstats = getStatID();

	//preciso criar comunicaçao com estatistcas
	while (1) {
		lerPipe(nome,comando);

		int credito=temCreditos(nome);
		printf("credito: %d\n",credito);
		if (credito==1) {
			printf("\nola\n");
			executaComando(nome,comando);
		}
		else {
			char lixo[MAX];
			char tmp[MAX];

			executaComando(nome,"echo exit");
		}
	}

	system("rm /tmp/comando /tmp/estatistica /tmp/saldo*");
	return 0;
}
