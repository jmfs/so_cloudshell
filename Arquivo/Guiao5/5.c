#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Escreva um programa que emule o funcionamento do interpretador de comandos na
 * execução encadeada 'grep -v ˆ# /etc/passwd | cut -f7 -d: | uniq | wc -l' */

int main ()
{
  int fd1[2];
  pipe(fd1); /* Criar o pipe */

  if (fork() == 0)              /* Filho */
  {
    int fd2[2];
    pipe(fd2);

    if (fork() == 0) {
      int fd3[2];
      pipe(fd3);

      if (fork() == 0) {
        dup2(fd3[1], 1);
        close(fd3[1]); close(fd3[0]);
        execlp("grep", "grep", "-v", "^#", "/etc/passwd", NULL);
      } else {
        dup2(fd3[0], 0);
        dup2(fd2[1], 1);
        close(fd3[0]); close(fd3[1]);
        close(fd2[0]); close(fd2[1]);
        execlp("cut", "cut", "-f7", "-d:", NULL);
      }
    } else {
      dup2(fd2[0], 0);
      dup2(fd1[1], 1);
      close(fd2[0]); close(fd2[1]);
      close(fd1[0]); close(fd1[1]);
      execlp("uniq", "uniq", NULL);
    }
  } else {                      /* Pai */
    dup2(fd1[0], 0);
    close(fd1[0]); close(fd1[1]);
    execlp("wc", "wc", "-l", NULL);
  }

  return 0;
}
