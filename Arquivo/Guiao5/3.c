#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Escreva um programa que execute o comando wc num processo filho. O processo pai deve
 * enviar ao filho através de um pipe anónimo uma sequência de linhas de texto introduzidas pelo
 * utilizador no seu standard input. Recorra à tecnica de redireccionamento estudada
 * no guiao anterior de modo a associar o standard input do processo filho ao descritor de
 * leitura do pipe anonimo criado pelo pai. Recorde a necessidade de fechar os descritores de 
 * escrita no pipe de modo a verificar-se a situação de end-of-file */

int main ()
{
  pid_t pid;
  char buffer[80];
  char * command = "wc";
  int fd[2], nbytes;

  pipe(fd);

  if ((pid = fork()) == 0)  /* Filho */
  {
    dup2(fd[0], 0);
    close(fd[1]); /* Fechar o std output do filho */
    close(fd[0]); /* Fechar o std input do filho */

    execlp(command, command, NULL);

    exit(0);
  } else {                  /* Pai */
    close(fd[0]); /* Fechar o std input do pai */

    while ((nbytes = read(0, buffer, sizeof(buffer))) != 0)
    {
      write(fd[1], buffer, nbytes);
    }

    close(fd[1]); /* Fechar o std output do pai */
  }
  return 0;
}
