#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main ()
{
  int fd[2];
  char c;

  pipe(fd);

  if (fork() != 0) { /* Filho */
    close(fd[0]);
    write(fd[1], "abc\n", 5);
    close(fd[1]);
  } else {
    close(fd[1]);
    while ( read(fd[0], &c, 1) > 0)
      write(1, &c, 1);
    close(fd[0]);
  }
  return(0);
}
