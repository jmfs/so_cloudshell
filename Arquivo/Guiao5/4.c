#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Escreva um programa que emule o funcionamento do interpretador de comandos na 
 * execução encadeada de 'ls /etc | wc -l' */

int main ()
{
  int fd[2], nbytes;
  char buffer[1024];  /* onde guardar o resultado de 'ls /etc' */

  pipe(fd);

  if ( fork() == 0 ) /* Filho */
  {
    dup2(fd[0], 0); /* Mudar std input para o descriptor de fd[0] */
    close(fd[0]);   /* Fechar input do filho */
    close(fd[1]);   /* Fechar output do filho */

    execlp("wc", "wc", "-l", NULL);

    exit(0);
  } else {
    dup2(fd[1], 1); /* Mudar std ouput para o output do pipe */
    close(fd[0]);   /* Fechar o input do pai */

    execlp("ls", "ls", "/etc", NULL);

    close(fd[1]);   /* Fechar o output do pai */
  }

  return 0;
}
