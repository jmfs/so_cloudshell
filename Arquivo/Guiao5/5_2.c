#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

/* Escreva um programa que emule o funcionamento do interpretador de comandos na
 * execução encadeada 'grep -v ˆ# /etc/passwd | cut -f7 -d: | uniq | wc -l' */

int main () {
  int fd1[2], fd2[2], fd3[2]; /* Descriptores para os pipes */
  int i;                      /* Iterador para controlo */
  pid_t pid = 1;

  pipe(fd1);
  pipe(fd2);
  pipe(fd3);

  for (i = 0; (i < 3) && (pid != 0); i++) {
    pid = fork();
  }

  if (!pid) {       /* Filhos */
    /* Terceiro filho */
    if (i == 3) {
      dup2(fd3[1], 1);
      close(fd3[1]); close(fd3[0]);
      close(fd2[1]); close(fd2[0]);
      close(fd1[1]); close(fd1[0]);
      execlp("grep", "grep", "-v", "^#", "/etc/passwd", NULL);
    }

    /* Segundo filho */
    if (i == 2) {
      dup2(fd3[0], 0);
      dup2(fd2[1], 1);
      close(fd3[1]); close(fd3[0]);
      close(fd2[1]); close(fd2[0]);
      close(fd1[1]); close(fd1[0]); 
      execlp("cut", "cut", "-f7", "-d:", NULL);
    }

    /* Primeiro filho */
    if (i == 1) {
      dup2(fd2[0], 0);
      dup2(fd1[1], 1);
      close(fd3[1]); close(fd3[0]);
      close(fd2[1]); close(fd2[0]);
      close(fd1[1]); close(fd1[0]);
      execlp("uniq", "uniq", NULL);
    }
  }

  if (pid) {        /* Pai */
    dup2(fd1[0], 0);                 /* Ler do pipe */
    close(fd1[1]); close(fd1[0]);
    close(fd2[1]); close(fd2[0]);
    close(fd3[1]); close(fd3[0]);
    
    execlp("wc", "wc", "-l", NULL);
  }

  return 0;
}

