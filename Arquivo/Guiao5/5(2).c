#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Escreva um programa que emule o funcionamento do interpretador de comandos na
 * execução encadeada 'grep -v ˆ# /etc/passwd | cut -f7 -d: | uniq | wc -l' */

int main () {
  int fd[2], i;
  pid_t pid;

  pipe(fd); /* Criar o pipe */

  for (i = 0; (i < 3) && pid; i++) {
    pid = fork();
  }
  
  if (!fork) {
    if (i == 1) {
      dup2(fd[1], 1);
      close(fd[1]); close(fd[0]);
      execlp("grep", "grep", "-v", "^#", "/etc/passwd", NULL);
    }

    if (i == 2) {
      dup2(fd[0], 0);
      dup2(fd[1], 1);
      close(fd[1]); close(fd[0]);
      execlp("cut", "cut", "-f7", "-d:", NULL);
    }

    if (i == 3) {
      dup2(fd[0], 0);
      dup2(fd[1], 1);
      close(fd[1]); close(fd[0]);
      execlp("uniq", "uniq", NULL);
    }
  }

  if (!fork && (i == 1)) {
    dup2(fd[0], 0);
    close(fd[1]); close(fd[0]);
    execlp("wc", "wc", "-l", NULL);
  }

  return 0;
}

