#include <sys/signal.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int fd[2];

void writer (int sig) {
  write(fd[1], "i", 2);
}

int main ()
{
  char c;

  pipe(fd);

  signal(SIGINT, writer);

  if ( fork() == 0 ) /* Filho */
  {
    write(fd[1], "i", 2);
  } else {
    while ( read(fd[0], &c, 1) > 0 )
      write(1, &c, 1);
  }

  return 0;
}

