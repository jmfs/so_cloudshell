#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int main ()
{
  int fd[2];
  char buffer[80], * message = "I am your son!";
  pid_t pid;

  /* Criar o pipe */
  pipe(fd);

  if ((pid = fork()) == 0)
  {
    close(fd[0]); /* Fechar o descriptor de leitura do filho */

    /* Escrever mensagem para o pai */
    write(fd[1], message, strlen(message) + 1);
    exit(0);
  } else {
    close(fd[1]); /* Fechar o descriptor de escrita do pai */

    sleep(1); /* Provocar atraso na recepção da mensagem */
    read(fd[0], buffer, sizeof(buffer));
    printf("Received: %s\n", buffer);
  }

  return 0;
}
