/* Simula a execução do pipe 'ps | sort| */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main () {
  int fd[2];

  pipe(fd);

  if (fork() == 0) {      /* Filho */
    dup2(fd[1], 1);               /* Duplicar descritor */
    close(fd[1]); close(fd[0]);   /* Fechar descritores do pipe */
    execlp("ps", "ps", NULL);     /* Executar o ps */
  } else {                /* Pai */
    dup2(fd[0], 0);               /* Duplicar descritor */
    close(fd[0]); close(fd[1]);   /* Fechar descritores do pipe */
    execlp("sort", "sort", NULL); /* Executar sort */
  }

  return 0;
}
