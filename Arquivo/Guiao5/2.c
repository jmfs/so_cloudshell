#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

/* Criar um pipe, fazer fork e fazer com que o pai faça leitura das mensagens do
 * filho enquanto não for detectade end of file.
 * Se ambos os processos tiverem o descritor de escrita aberta o read
 * não detecta o end-of-file e fica a espera de input. Isto acontece apenas quando 
 * tanto pai como filho tem o descritor de escrita aberto */

int main ()
{
  pid_t pid;
  int fd[2], nbytes;
  char buffer[80];

  /* Criar o pipe */
  pipe(fd);

  if ((pid = fork()) == 0)
  {
    close(fd[0]); /* Fechar descriptor de leitura do filho */

    write(fd[1], "Message 1\n", strlen("Message 1\n"));
    write(fd[1], "Message 2\n", strlen("Message 2\n"));
    write(fd[1], "Message 2\n", strlen("Message 2\n"));

    close(fd[1]); /* Fechar descriptor de escrita do filho */
    exit(0);
  } else {
    close(fd[1]); /* Fechar o descriptor de escrita do pai e fazer com que quando o processo
                     filho termine o read detecte end-of-file */

    while ((nbytes = read(fd[0], buffer, sizeof(buffer))) != 0)
    {
      printf("%s", buffer);
    }

    close(fd[0]); /* Fechar descriptor de leitura do pai */
  }

  return 0;
}
