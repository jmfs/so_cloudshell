#include <sys/signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
typedef void (*sighandler_t)(int);

clock_t start, elapsed;  /* Beginnig time, finish time, time elapsed */
int done = 0, counter = 0;

void sigint_handler (int sig) {
  counter++;
  elapsed = (double)(clock() - start)/CLOCKS_PER_SEC;
  printf("%2.5d seconds elapesd\n", elapsed);
  start = clock();
}

void sigquit_handler (int sig) {
  done = 1;
}

int main (int argc, char ** argv) {
  signal(SIGINT, sigint_handler);
  signal(SIGQUIT, sigquit_handler);
  start = clock();  // Set starting time

  while (!done)
    ;

  printf("User pressed Ctrl-C %d times\n", counter);
}
