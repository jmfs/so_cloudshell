#include <sys/signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
typedef void (*sighandler_t)(int);

void alarm_handler (int sig) {
  signal(SIGALRM, SIG_IGN);
  printf("Hello\n");
  signal(SIGALRM, alarm_handler);
}

int main (int argc, char ** argv) {
  signal(SIGALRM, alarm_handler);
  alarm(2);
  while (1);
  printf("All done\n");
}
