#include <stdio.h>
#include <signal.h>
#include <sys/signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

int counter = 0, children;
pid_t fpids[10];

void sigchld_handler (int signum) {
  pid_t chpid = waitpid(-1, NULL, 0);

  for (int i = 0; i < children; i++)
    if (fpids[i] == chpid) { fpids[i] = 0; counter++; }
}

int main (int argc, char ** argv) {
  int it = 0;
  pid_t fpid;

  children = argc - 1;

  for (it = 0; (it < children) && (fpid != 0); it++) {
    printf("111\n");
    fpid = fork();
    if (fpid) fpids[it] = fpid;
  }

  return 0;
}
