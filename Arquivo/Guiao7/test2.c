#include <signal.h>
#include <sys/types.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>
typedef void (*sighandler_t)(int);

clock_t start, stop;
int fpid, status;

void sigchld_handler (int sig) {
  printf("O meu filho acabou!\n");
  waitpid(fpid, &status, 0);
}

void sigalrm_handler (int sig) {
  printf("Chato!\n");
}

int main (int argc, char ** argv) {

  signal(SIGCHLD, sigchld_handler);
  fpid = fork();

  if (fpid == 0) {  /* Filho */
    printf("%d\n", getpid());
    for (int i = 0; i < 100; i++) ;
    _exit(4);
  } else {          /* Pai */
    sleep(5);
    printf("Acordei , %d\n", fpid);
    sleep(1);
  }
  return 0;
}
